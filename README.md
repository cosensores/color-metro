
Herramienta libre para cuantificar la concentración de analítos coloreados en solución, a partir de la determinación de valores de absorbancia aplicando luz led RGB.

Adaptación del colorimetro de ColorLabFD: https://gitlab.com/nanocastro/ColorLabFD

﻿![foto](/Imagenes/colorimetro1.jpg)
﻿![foto](/Imagenes/colorimetro2.jpg)

Adaptación bluetooth del colorímetro LabFD

﻿![foto](/Imagenes/COLORimetro_bb.jpg)

﻿Aplicación: /Aplicación android/colorimetro.apk  
﻿Aplicación editable[(kodular)](https://www.kodular.io/):/Aplicación android/colorimetro.aia
